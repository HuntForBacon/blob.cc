package com.blobcc.main.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.blobcc.main.Application;

public class DesktopLauncher {
	
	public static final String TITLE = "Blobs.cc - Indev";
	public static final String GAME_DIRECTORY = System.getProperty("user.home") + "/blobcc";
	public static final int VIRTUAL_WIDTH = 1280;
	public static final int VIRTUAL_HEIGHT = 720;
	private static OptionsConfig options;
	
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		options = new OptionsConfig();
		
		config.title = TITLE;
		config.width = VIRTUAL_WIDTH;
		config.height = VIRTUAL_HEIGHT;
		config.resizable = true;
		config.foregroundFPS = options.getFps();
		config.vSyncEnabled = options.getVsyncEnabled();
		config.samples = options.getMSAA(); // Optional MSAA
		
		new LwjglApplication(new Application(options.getOptionsListener()), config);
	}
}