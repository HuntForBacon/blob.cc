package com.blobcc.main.desktop;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import org.json.JSONObject;

public class OptionsFileLoader {

	public JSONObject getSettings(){
		
		File file = new File(DesktopLauncher.GAME_DIRECTORY);
		
		if(!file.exists()){
			return new JSONObject();
		}
			
		try(BufferedReader br = new BufferedReader(new FileReader(file + "/options.json"))){
			String currentLine;
			
			StringBuilder sb = new StringBuilder();
			while((currentLine = br.readLine()) != null){
				sb.append(currentLine);
			}
			
			JSONObject jsonData = new JSONObject(sb.toString());
			System.out.println("Reading complete");
			return jsonData;
			
		}catch(Exception ex){
			System.out.println("ERROR MISSING FILE: " + file.getPath());
		}
		return null;
	}
}
