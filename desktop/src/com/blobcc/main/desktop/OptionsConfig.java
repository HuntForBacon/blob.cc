package com.blobcc.main.desktop;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JOptionPane;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 
 * @author Snessy
 * @version 1.0 build indev
 * @since 06/11/2015
 * 
 */

//TODO need to save the settings. Likely use json
public class OptionsConfig implements OptionsListener {

	private OptionsFileLoader optionsLoader;
	private JSONObject jsonData;

	/**
	 * @param config Configuration instance from the LwjglApplicationConfiguration class
	 */
	public OptionsConfig(){
		
		optionsLoader = new OptionsFileLoader();
		jsonData = optionsLoader.getSettings();
	}
	
	public void saveSettings(){
		
		File folder = new File(DesktopLauncher.GAME_DIRECTORY);
		folder.mkdir();
		
		try(FileWriter file = new FileWriter(folder + "/options.json")){
			
			file.write(jsonData.toString());
			System.out.println("Done");
			
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Failed writing to file: " + folder.toString() + "\n" + e, "Error saving settings", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	/**
	 * @param msaa The selected MSAA that the user wishes to use
	 */
	@Override
	public void setMsaa(int msaa) {
		jsonData.put("msaa", msaa);
	}
	
	@Override
	public void setFps(int fps) {
		jsonData.put("fps", fps);
	}
	
	@Override
	public void setVsyncEnabled(boolean isEnabled) {
		jsonData.put("vsyncEnabled", isEnabled);
	}
	
	public boolean getVsyncEnabled(){
		try {
			return jsonData.getBoolean("vsyncEnabled");
		} catch (JSONException e) {
			return true;
		}
	}
	
	public int getMSAA(){
		try {
			return jsonData.getInt("msaa");
		} catch (JSONException e) {
			return 0;
		}
	}
	
	public int getFps(){
		try {
			return jsonData.getInt("fps");
		} catch (JSONException e) {
			return 60;
		}
	}
	
	public OptionsListener getOptionsListener(){
		return this;
	}
}