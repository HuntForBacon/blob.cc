package com.blobcc.main.desktop;

public interface OptionsListener {
	
	public void saveSettings();
	public void setMsaa(int msaa);
	public void setFps(int fps);
	public void setVsyncEnabled(boolean isEnabled);
}
