# Setup! #

Pretty simple setup for this one. You simply need to download the [Gradle](https://marketplace.eclipse.org/content/gradle-integration-eclipse-0) plugin for your IDE of choice (go team [Eclipse](https://eclipse.org/downloads/)!), clone down this repo and then import it as a Gradle project.
If you have any issues them message Snessy on Skype or ContortedPixelinfo@gmail.com and i'll guide you through the setup.

## Java 8 ##
The project is now using Java 8 so you must make sure that you have Java 8 installed on your machine AND that you set you Java compiler to use 1.8

# URGENT Update your Gradle dependencies! #
We are now using some 3rd party jar files so if you already have the code in your favourite IDE then follow these instructions...
Right click blobcc-desktop->Gradle->Refresh dependencies

You may also wish to do this on blob and blobcc-core as well just incase we update anything else.

# URGENT #

Due to a config change... I've messed up the project and i'm working on an urgent fix... but it will really bring cool features to the game.
When you pull down the latest version of develop the code won't run so you need to go through the following steps...
* Right click on blobcc-core and select build path -> config build path -> projects -> add -> select the blobcc-desktop project
* After this you need to change some compiler options on both blobcc-core and blobcc-desktop. Right click on which ever you prefer first and follow these settings... Properties -> Java compiler -> Building -> change Circular Dependencies from error to warning