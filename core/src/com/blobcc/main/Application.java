package com.blobcc.main;

import com.badlogic.gdx.Game;
import com.blobcc.main.desktop.OptionsListener;
import com.blobcc.screens.GameScreen;

public class Application extends Game{
	
	public static final int VIRTUAL_WIDTH = 1280;
	public static final int VIRTUAL_HEIGHT = 720;
	public static OptionsListener optionsListener;

	public Application(OptionsListener optionsListener) {
		Application.optionsListener = optionsListener;
	}

	@Override
	public void create() {
		
		// Load in all the assets for the game before the mainmenu loads
		Assets.loadAssets();
		//setScreen(new MainMenu(this));
		setScreen(new GameScreen(this));
	}
}