package com.blobcc.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class Player {
	
	private float xPos;
	private float yPos;
	private int mass;
	private Color playerColor;
	private Circle bounds;
	private ShapeRenderer renderer;
	
	public Player(){
		//TODO Random starting position 
		xPos = 500f;
		yPos = 500f;
		mass = 20;
		bounds = new Circle(xPos, yPos, mass);
		
		renderer = new ShapeRenderer();
		
		generateRandomColor();
	}
	
	// Just a piece of fun code? :P
	private void generateRandomColor() {
		
	}

	public void Update(OrthographicCamera camera){
		
		renderer.setProjectionMatrix(camera.combined);
		
		camera.position.set(xPos, yPos, 0);
		
		renderer.begin(ShapeType.Filled);
		renderer.circle(xPos, yPos, mass);
		renderer.circle(200, 200, 100);
		renderer.setColor(Color.BLUE);
		renderer.end();
		
		movement(camera);
	}
	
	public void movement(OrthographicCamera camera){
		
		Vector3 screenCords = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
		Vector3 worldCords = camera.unproject(screenCords);
		
		float diffX = worldCords.x - xPos;
		float diffY = worldCords.y - yPos;
		
		Vector2 vec = new Vector2(diffX, diffY);
		vec.nor();
		
		int speed = (int) (60 - (mass / 12));
		
		double squaredDistance = (diffX*diffX + diffY*diffY);
		double distance = Math.sqrt(squaredDistance);
		
		// This stops the player bouncing around when the mouse isn't moving
		if(distance > 5){
			xPos += (vec.x * speed) * Gdx.graphics.getDeltaTime();
			yPos += (vec.y * speed) * Gdx.graphics.getDeltaTime();
			
		}
	}
}
