package com.blobcc.screens;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.blobcc.main.Application;

/*
 * To whomever builds this class file up... You'll need to research about Scene2D and the basics of getting buttons/icons to render
 * Currently the skin is empty.. so were are going to need a TextureAtlas containing all the relevant buttons etc
 * 
 * I @snessy can likely use one of my old TextureAtlas's for the the time being but we will need a designer to work on new art
 * 
 */

public class SettingsScreen implements Screen {
	
	private Stage stage;
	private Skin skin;
	
	public SettingsScreen(){
		stage = new Stage(new StretchViewport(Application.VIRTUAL_WIDTH, Application.VIRTUAL_HEIGHT));
		skin = new Skin();
	}

	@Override
	public void show() {
		
	}

	@Override
	public void render(float delta) {
		
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height);
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void hide() {
		
	}

	@Override
	public void dispose() {
		stage.dispose();
		skin.dispose();
	}

}
