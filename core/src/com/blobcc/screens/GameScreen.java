package com.blobcc.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.blobcc.entities.Player;
import com.blobcc.main.Application;

public class GameScreen implements Screen {

	private Application game;
	private OrthographicCamera camera;
	private Player player;

	//TODO Fix the resize issue to maintain the same aspect ratio
	public GameScreen(Application application) {
		this.game = application;
		player = new Player();
		camera = new OrthographicCamera(Application.VIRTUAL_WIDTH, Application.VIRTUAL_HEIGHT);
		camera.setToOrtho(false);
	}

	@Override
	public void show() {
		
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(1f, 1f, 1f, 1f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		player.Update(camera);
		camera.update();
	}

	@Override
	public void resize(int width, int height) {
		camera.viewportHeight = height;
		camera.viewportWidth = width;
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void hide() {
		
	}

	@Override
	public void dispose() {
		
	}

}